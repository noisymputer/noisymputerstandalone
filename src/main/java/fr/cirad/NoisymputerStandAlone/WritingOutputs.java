/*******************************************************************************
 * NOISYmputer
 * Distributed under the GNU Affero General Public License V3.0
 * See <https://www.gnu.org/licenses/agpl-3.0.en.html> for details
 ******************************************************************************/
package fr.cirad.NoisymputerStandAlone;

import fr.cirad.NoisymputerStandAlone.NoisymputerProperties.ExportFormat;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.StringJoiner;
import java.util.stream.Collectors;
import noisymputer.AlienSegment;
import noisymputer.AllDataInBytes;
import static noisymputer.AllDataInBytes.genHTZ;
import static noisymputer.AllDataInBytes.genMIS;
import static noisymputer.AllDataInBytes.genPA1;
import static noisymputer.AllDataInBytes.genPA2;
import noisymputer.BreakPoint;
import noisymputer.Chunk;
import noisymputer.Functions;
import noisymputer.RefAlleles;
import noisymputer.SampleStats;
import noisymputer.GeneticMap;
import noisymputer.Probas;
import noisymputer.Snp;
import noisymputer.SnpSelectionCriteria;
import noisymputer.Zone;

/**
 *
 * @author boizet
 */
public class WritingOutputs {
    
    private AllDataInBytes data;
    private List<String> sampleNames;
    private String chr;

    public WritingOutputs(AllDataInBytes data, String chr) {
        this.data = data;
        sampleNames = data.getSamplesNames();
        this.chr = chr;
    }


    public AllDataInBytes getData() {
        return data;
    }

    public void setData(AllDataInBytes data) {
        this.data = data;
    } 
    
    public void writeFile(String outputFilePath, ExportFormat format) throws IOException {
        writeFile(outputFilePath, format, false);
    }
    
    public void writeFile(String outputFilePath, ExportFormat format, boolean collapsed) throws IOException {
        writeFile(outputFilePath, format, collapsed, false);
    }
    
    /**
     * Write genotypes file in different format (VCF, ABH, byte or hapmap)
     * @param outputFilePath
     * @param format
     * @param collapsed to know if we get data from collapsing data or not
     * @param append to know if we write in the same file (to have all chromosome in the same file)
     * @throws IOException 
     */
    public void writeFile(String outputFilePath, ExportFormat format, boolean collapsed, boolean append) throws IOException {
        switch (format) {
            case CSV_ABH:
                outputFilePath = outputFilePath + ".csv";
                writeCsvFile(outputFilePath, true, collapsed, append);
                break;
            case CSV_BYTE:
                outputFilePath = outputFilePath + ".csv";
                writeCsvFile(outputFilePath, false, collapsed, append);
                break;
            case HAPMAP:
                outputFilePath = outputFilePath + ".hapmap";
                writeHapMap(outputFilePath, collapsed, append);
                break;
            case VCF:
                outputFilePath = outputFilePath + ".vcf";                
                writeVCF(outputFilePath, collapsed, append);      
                break;
            default:
                System.out.println("ERROR - exportFormat unknown");
                break;
        }

    }
    
    /**
     * Write genotypes file in CSV 
     * 2 available formats : ABH-, or bytes (1,2,3,9)
     * @param outputFilePath
     * @param ABHformat ABH if true, bytes if false
     * @param collapsed to know if we get data from collapsing data or not
     * @param append to know if we write in the same file (to have all chromosome in the same file)
     * @throws IOException 
     */
    public void writeCsvFile(String outputFilePath, boolean ABHformat, boolean collapsed, boolean append) throws IOException {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(outputFilePath, append))) {
            if (!append) {
                writer.write("chr,position,snp," + String.join(",", this.sampleNames));
                writer.newLine();
            }

            ArrayList<Snp> snpData = collapsed ? data.getSnpDataCollapsed() : data.getSnpData();
            for (int v = 0; v < snpData.size(); v++) {                    
                String genotypes;
                if (ABHformat) {
                    String[] snpInABH = Functions.convertBytesToABH(snpData.get(v).getGenotypes());
                    genotypes = Arrays.toString(snpInABH).substring(1);
                } else {
                    genotypes = Arrays.toString(snpData.get(v).getGenotypes()).substring(1);
                }
                genotypes = genotypes.substring(0, genotypes.length() - 1);
                genotypes = genotypes.replaceAll(" ", "");
                String line =  this.chr + "," + snpData.get(v).getPosition() + "," + snpData.get(v).getName() + "," + genotypes;
                writer.write(line);
                writer.newLine();
            }
            
            writer.close();
        } catch (Exception e) {
            System.out.println("Error writing output file");
            System.out.println(e.getMessage());
        }
    }
    
    /** 
     * Write a CSV file with genotypic frequencies for each SNP
     * @param outputFilePath
     * @throws IOException 
     */
    public void writeFrequencies(String outputFilePath) throws IOException {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(outputFilePath))) {
            String header = "chr,position,snp,A,B,H,MD,reads";
            writer.write(header);
            writer.newLine();
   
            data.updateAlleleCounts(true);
            ArrayList<Snp> snpData = data.getSnpData(); 
            for (int i = 0; i < snpData.size(); i++) {
                double freqA = (double) snpData.get(i).getAlleleCounts().getNbA() / snpData.get(i).getAlleleCounts().getTotalCount();
                double freqB = (double) snpData.get(i).getAlleleCounts().getNbB() / snpData.get(i).getAlleleCounts().getTotalCount();
                double freqH = (double) snpData.get(i).getAlleleCounts().getNbH()/ snpData.get(i).getAlleleCounts().getTotalCount();
                double freqMD = (double) snpData.get(i).getAlleleCounts().getNbMD() / snpData.get(i).getAlleleCounts().getTotalCount();

                //count reads
                int reads = 0;
                for (int s = 0; s < data.getSamplesNames().size(); s++) {
                    reads = reads + snpData.get(i).getNbReadsA()[s] + snpData.get(i).getNbReadsB()[s];
                }

                String line = this.chr + "," + snpData.get(i).getPosition() + "," + snpData.get(i).getName()
                        + "," + freqA + "," + freqB + "," + freqH + "," + freqMD+ "," + reads;
                writer.write(line); 
                writer.newLine();
            }
            
            writer.close();
        } catch (Exception e) {
            System.out.println("Error writing frequencies file");
            System.out.println(e.getMessage());
        }
    }
    
    /**
     * Write a CSV file with the detected breakpoints
     * @param outputFilePath
     * @throws IOException 
     */
    public void writeBreakPoints(String outputFilePath) throws IOException {  
        for (int s = 0; s < sampleNames.size(); s++) {
            List<BreakPoint> sampleBkp = data.getBkps().get(s);
            if (sampleBkp != null) {
                for (int bkp = 1; bkp < sampleBkp.size(); bkp++) {
                    if ((sampleBkp.get(bkp).getTransitionType().startsWith("H") && sampleBkp.get(bkp - 1).getTransitionType().startsWith("H"))
                            || (sampleBkp.get(bkp).getTransitionType().endsWith("H") && sampleBkp.get(bkp - 1).getTransitionType().endsWith("H"))) {
                        System.out.println("error in bkps : sample " + sampleNames.get(s) + " , bkp : " + sampleBkp.get(bkp).getStartBkpPosition());
                    }
                }
            }
        }
        
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(outputFilePath))) {
            String sep = ",";
            
            StringJoiner header = new StringJoiner(sep);
            header.add("sample");
            header.add("chr");
            header.add("average_bkp_position");
            header.add("bkp_start_position");
            header.add("bkp_stop_position");
            header.add("found_bkp");
            header.add("bkp_size");
            header.add("loose_interval_start_position");
            header.add("loose_interval_stop_position");
            header.add("found_loose_interval");
            header.add("SNPs_nb_in_loose_interval");
            header.add("data_nb_in_loose_interval");
            header.add("support_interval_start_position");
            header.add("support_interval_stop_position");
            header.add("SNPs_nb_in_support_interval");
            header.add("data_nb_in_support_interval");
            header.add("SNPs_nb_in_HMZ_window");
            header.add("SNPs_nb_in_H_window");
            header.add("transitionType");
            writer.write(header.toString());
            writer.newLine();            
           
            List<BreakPoint> bkpList = data.getBkps().values().stream().flatMap(Collection::stream).collect(Collectors.toList());
            for (BreakPoint bkp:bkpList) {
                StringJoiner line = new StringJoiner(sep);
                line.add(this.sampleNames.get(bkp.getSampleNumber()));
                line.add(bkp.getChr());
                line.add(bkp.getAverageBkpPosition() != null ? bkp.getAverageBkpPosition().toString() : "");
                line.add(bkp.getStartBkpPosition() != null ? bkp.getStartBkpPosition().toString() : "");
                line.add(bkp.getStopBkpPosition() != null ? bkp.getStopBkpPosition().toString() : "");
                line.add(bkp.getFoundBkp() != null ? bkp.getFoundBkp().toString() : "");
                line.add(bkp.getBkpLength() != null ? bkp.getBkpLength().toString() : "");
                line.add(bkp.getStartLooseInterval() != null ? bkp.getStartLooseInterval().toString() : "");
                line.add(bkp.getStopLooseInterval() != null ? bkp.getStopLooseInterval().toString() : "");
                line.add(bkp.getFoundLooseInterval() != null ? bkp.getFoundLooseInterval().toString() : "");
                line.add(bkp.getSnpsNbInLooseInterval() != null ? Integer.toString(bkp.getSnpsNbInLooseInterval()) : "");
                line.add(bkp.getDataNbInLooseInterval() != null ? Integer.toString(bkp.getDataNbInLooseInterval()) : "");
                line.add(bkp.getStartSupportInterval() != null ? bkp.getStartSupportInterval().toString() : "");
                line.add(bkp.getStopSupportInterval() != null ? bkp.getStopSupportInterval().toString() : "");
                line.add(bkp.getSnpsNbInSupportInterval() != null ? Integer.toString(bkp.getSnpsNbInSupportInterval()) : "");
                line.add(bkp.getDataNbInSupportInterval() != null ? Integer.toString(bkp.getDataNbInSupportInterval()) : "");
                line.add(bkp.getSnpsNbInHMZWindow() != null ? bkp.getSnpsNbInHMZWindow().toString() : "");
                line.add(bkp.getSnpsNbInHWindow() != null ? bkp.getSnpsNbInHWindow().toString() : "");
                line.add(bkp.getTransitionType());
                writer.write(line.toString());
                writer.newLine();
            }            
            writer.close();
        } catch (Exception e) {
            System.out.println("Error writing breakpoints file");
            if (e.getMessage() != null) {
                System.out.println(e.getMessage());
            }
        }
    }
    
    /**
     * Write a CSV file with genotypic frequencies, and number of reads for each sample
     * @param outputFilePath
     * @throws IOException 
     */
    public void writeSampleStats(String outputFilePath) throws IOException {
        
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(outputFilePath))) {
            String sep = ",";
            
            StringJoiner header = new StringJoiner(sep);
            header.add("Chr");
            header.add("Sample");
            header.add("%MD");
            header.add("%HTZ");
            header.add("%A");
            header.add("%B");
            header.add("reads");
            header.add("transitions_nb");
            
   
            ArrayList<SampleStats> stats = data.calcSamplesStats();
            writer.write(header.toString());
            writer.newLine();
             
            for (int i = 0; i < sampleNames.size(); i++) {
                StringJoiner line = new StringJoiner(sep);
                line.add(this.chr);
                line.add(sampleNames.get(i));
                line.add(String.valueOf(stats.get(i).getFreqMD()));
                line.add(String.valueOf(stats.get(i).getFreqH()));
                line.add(String.valueOf(stats.get(i).getFreqA()));
                line.add(String.valueOf(stats.get(i).getFreqB()));
                line.add(String.valueOf(stats.get(i).getReads()));

                if (data.getBkps().get(i) != null) {
                    line.add(String.valueOf(data.getBkps().get(i).size()));
                } else {
                    line.add("0");
                }

                writer.write(line.toString());
                writer.newLine();
            }
            
            writer.close();
        } catch (Exception e) {
            System.out.println("Error writing sampleStats file");
            if (e.getMessage() != null) {
                System.out.println(e.getMessage());
            }
        }
        
    }
    
    /**
     * Write a CSV with the positions of detected aliens
     * @param outputFilePath
     * @throws IOException 
     */
    public void writeAliens(String outputFilePath) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(outputFilePath));
        String sep = ",";
        StringJoiner header = new StringJoiner(sep);
        header.add("Chr");
        header.add("startLocus");
        header.add("startPosition");
        header.add("endLocus");
        header.add("endPosition");
        header.add("highSlopeSNP");
        header.add("offset");
        header.add("HS1");
        header.add("HS2");
        header.add("minRf");
        writer.write(header.toString());
        writer.newLine();

        if (data.getAliens() != null) {
            for (AlienSegment alien:data.getAliens()) { 
                StringJoiner line = new StringJoiner(sep);
                line.add(alien.getChr());
                line.add(String.valueOf(alien.getStart()));
                line.add(String.valueOf(alien.getStartPosition()));
                line.add(String.valueOf(alien.getStop()));
                line.add(String.valueOf(alien.getStopPosition()));
                line.add(String.valueOf(alien.getHighSlopeSNP()));
                line.add(String.valueOf(alien.getOffset()));
                line.add(String.valueOf(alien.getHS1()));
                line.add(String.valueOf(alien.getHS2()));
                line.add(Double.toString(alien.getMinRF()));

                writer.write(line.toString()); 
                writer.newLine();
            }     
        }
        
        writer.close();
    }
    
    /**
     * Write the genetic map in a CSV file
     * @param outputFilePath 
     */
    public void writeGeneticMapToCSV(String outputFilePath) {
        final String sep = ",";
        String header = "chr" + sep + "position" + sep + "snpName" + sep + "F1Rec" + sep + "RF" + sep + "cumulated_cM";
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(outputFilePath))) {
            writer.write(header);
            writer.newLine();

            ArrayList<Snp> snpData = data.getSnpData(); 
            for (int v = 0; v < snpData.size(); v++) {
                GeneticMap map = data.getGeneticMap().get(v);
                String line = this.chr + sep + snpData.get(v).getPosition() + sep +  snpData.get(v).getName()
                        + sep + map.getF1Rec() + sep + map.getRf() + sep + map.getcM();
                writer.write(line);
                writer.newLine();
            }
            
            writer.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
    public void writeMareyMapToCSV(String outputFilePath, SnpSelectionCriteria ssc) throws IOException {
        writeMareyMapToCSV(outputFilePath, ssc, false, true);
    }

    /**
     * Write the genetic map in Marey map format
     * @param outputFilePath
     * @param ssc
     * @param collapsed to know if we get data from collapsing data or not
     * @throws IOException 
     */
    public void writeMareyMapToCSV(String outputFilePath, SnpSelectionCriteria ssc, boolean collapsed, boolean transitionsNbColumn) throws IOException {
        
        final String sep = ",";
        StringJoiner header = new StringJoiner(sep);
        header.add("chr");
        header.add("position");
        header.add("snpName");
        header.add("nbA");
        header.add("nbB");
        header.add("nbH");
        header.add("ps_cM");
        header.add("F1Rec");
        header.add("RF");
        header.add("cumulated_cM");
        header.add("cMbp");
        header.add("slope");
        header.add("slope2");
        
        int[] transitionsNbPerSite = null;
        if (transitionsNbColumn) {
            header.add("#transitions"); 
            if (collapsed) {
                transitionsNbPerSite = data.calcTransitionsNbPerSite(true);
            } else {
                transitionsNbPerSite = data.calcTransitionsNbPerSite(false);
            }
        }               

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(outputFilePath))) {
            writer.write(header.toString());
            writer.newLine();

            ArrayList<Snp> snpData = collapsed ? data.getSnpDataCollapsed() : data.getSnpData(); 
            for (int v = 0; v < snpData.size(); v++) {
                GeneticMap map = data.getGeneticMap().get(v);
                double pcM = (double) snpData.get(v).getPosition() / ssc.getBpPercM();
                StringJoiner line = new StringJoiner(sep);
                line.add(this.chr);
                line.add(snpData.get(v).getPosition().toString());
                line.add(snpData.get(v).getName());
                line.add(Integer.toString(snpData.get(v).getAlleleCounts().getNbA()));
                line.add(Integer.toString(snpData.get(v).getAlleleCounts().getNbB()));
                line.add(Integer.toString(snpData.get(v).getAlleleCounts().getNbH()));
                line.add(Double.toString(pcM));
                line.add(Double.toString(map.getF1Rec()));
                line.add(Double.toString(map.getRf()));
                line.add(Double.toString(map.getcM()));
                line.add(Double.toString(map.getcMbp()));
                line.add(Double.toString(map.getSlope()));
                line.add(Double.toString(map.getSlope2()));
                if (transitionsNbPerSite != null) {
                    line.add(Integer.toString(transitionsNbPerSite[v])); 
                }
                
                writer.write(line.toString());
                writer.newLine();
            }
            
            writer.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /** 
     * Write genotypes file in hapmap format
     * @param outputFilePath
     * @param collapsed to know if we get data from collapsing data or not
     * @param append to know if we write in the same file (to have all chromosome in the same file)
     * @throws IOException 
     */
    public void writeHapMap(String outputFilePath, boolean collapsed, boolean append) throws IOException {
        final String sep = "\t";
        String header = "rs#" + sep + "alleles" + sep + "chrom" + sep + "pos" + sep + "strand" + sep + "assembly#" 
                + sep + "center" + sep + "protLSID" + sep + "assayLSID" + sep + "panelLSID" + sep + "QCcode";
        
        for (int s=0; s<this.sampleNames.size(); s++) {
                header = header + sep + this.sampleNames.get(s);
        }
        
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(outputFilePath, append))) {
            if (!append) {
                writer.write(header);
                writer.newLine();
            }
            
            ArrayList<Snp> snpInfo = collapsed ? data.getSnpDataCollapsed() : data.getSnpData();
            for (int v=0; v < snpInfo.size(); v++) {
                RefAlleles snpAlleles = snpInfo.get(v).getAlleles();
                String ref = snpAlleles.getRefAltAlleles()[0];
                String alt = snpAlleles.getRefAltAlleles()[1];
                String refAlleles = ref + "/" + alt;
                String p1 = snpAlleles.getParent1Allele();
                String p2 = snpAlleles.getParent2Allele();

                String line = snpInfo.get(v).getName() + sep + refAlleles + sep + this.chr + sep + snpInfo.get(v).getPosition()
                        + sep + "+" + sep + "NA" + sep + "NA" + sep + "NA" + sep + "NA" + sep + "NA" + sep + "NA";
                for (int s=0; s<sampleNames.size(); s++) {
                    byte gt = data.getSnpData().get(v).getGenotypes()[s];
                    String gtStr;
                    if (gt == genPA1) {
                        gtStr = p1 + p1;
                    } else if (gt == genPA1) {
                        gtStr = p2 + p2;
                    } else if (gt == genHTZ) {
                        if (ref.equals(p1)) {
                            gtStr = p1 + p2;
                        } else 
                            gtStr = p2 + p1;
                    } else {
                        gtStr = "."; //missing data
                    }
                    line = line + sep + gtStr;
                }
                writer.write(line);
                writer.newLine();
            }
            
            writer.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /** 
     * Write genotypes file in VCF format
     * @param outputFilePath
     * @param collapsed to know if we get data from collapsing data or not
     * @param append to know if we write in the same file (to have all chromosome in the same file)
     * @throws IOException 
     */
    public void writeVCF(String outputFilePath, boolean collapsed, boolean append) throws IOException {
        final long startTime = System.currentTimeMillis();
        final String sep = "\t";
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(outputFilePath, append))) {
            StringJoiner joiner;
            if (!append) {
                LocalDateTime ldt = LocalDateTime.now();
                String currentDate = DateTimeFormatter.ofPattern("yyyyMMdd", Locale.ENGLISH).format(ldt);

                writer.write("##fileformat=VCFv4.2");
                writer.newLine();
                writer.write("##fileDate=" + currentDate);
                writer.newLine();
                writer.write("##source=NoisymputerStandAlone");
                writer.newLine();
                //writer.write("##reference=");
                //writer.newLine();
    //            for (String chr:data.keySet()) {
    //                writer.write("##contig=<ID=" + chr +">");
    //                writer.newLine();
    //            }
                writer.write("##phasing=none");
                writer.newLine();
                writer.write("##FORMAT=<ID=GT,Number=1,Type=String,Description=\"Genotype\">");
                writer.newLine();
                writer.write("##FORMAT=<ID=DP,Number=1,Type=Integer,Description=\"Read Depth\">");
                writer.newLine();
                writer.write("##FORMAT=<ID=AD,Number=R,Type=Integer,Description=\"Number of observation for each allele\">");                



                writer.newLine();
                joiner = new StringJoiner(sep);
                joiner.add("#CHROM");
                joiner.add("POS");
                joiner.add("ID");
                joiner.add("REF");
                joiner.add("ALT");
                joiner.add("QUAL");
                joiner.add("FILTER");
                joiner.add("INFO");
                joiner.add("FORMAT");
                for (String sample:this.sampleNames)  {
                    joiner.add(sample);
                }
                writer.write(joiner.toString());
                writer.newLine();
            }
            

            ArrayList<Snp> snpData = collapsed ? data.getSnpDataCollapsed() : data.getSnpData();
            for (int v = 0; v < snpData.size(); v++) {
                joiner = new StringJoiner(sep);
                joiner.add(this.chr);
                joiner.add(snpData.get(v).getPosition().toString());
                joiner.add(snpData.get(v).getName());

                RefAlleles snpAlleles = snpData.get(v).getAlleles();
                String ref = snpAlleles.getRefAltAlleles()[0];
                String alt = snpAlleles.getRefAltAlleles()[1];
                String p1 = snpAlleles.getParent1Allele();
                String p2 = snpAlleles.getParent2Allele();

                String gtP1;
                String gtP2;
                String gtHTZ;
                if (p1.equals(ref)) {
                    gtP1 = "0/0";
                    gtP2 = "1/1";
                    gtHTZ = "0/1";
                } else {
                    gtP1 = "1/1";
                    gtP2 = "0/0";
                    gtHTZ = "1/0";
                }


                joiner.add(ref);
                joiner.add(alt);
                joiner.add(".");
                joiner.add(".");
                joiner.add(".");
                joiner.add("GT:DP:AD");
                for (int s = 0; s<this.sampleNames.size(); s++) {
                    byte allele = snpData.get(v).getGenotypes()[s];
                    String gt = ".";
                    switch (allele) {
                        case genPA1:                            
                            gt = gtP1;
                            break;
                        case genPA2:
                            gt = gtP2;
                            break;
                        case genHTZ:
                            gt = gtHTZ;
                            break;
                    } 
                    String ad = snpData.get(v).getNbReadsA()[s] + "," + snpData.get(v).getNbReadsB()[s];
                    String dp = String.valueOf(snpData.get(v).getNbReadsA()[s] + snpData.get(v).getNbReadsB()[s]);
                    joiner.add(gt + ":" + dp + ":" + ad);
                    
                }
                writer.write(joiner.toString());
                writer.newLine();
            }
        }
        
        final long endTime = System.currentTimeMillis();
        //System.out.println("execution time VCF1: " + (endTime - startTime));
        
    }

    //FOR TESTING PURPOSES
    /** 
     * Write genotypes, reads for one sample
     * @param outputFilePath
     * @param ABHformat
     * @param sampleName 
     */
    public void writeCsvFileForOneSample(String outputFilePath, boolean ABHformat, String sampleName) {
        
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(outputFilePath + "_" + sampleName + ".csv"))) {
            String sep = ",";
            
            StringJoiner header = new StringJoiner(sep);
            
            header.add("chr");
            header.add("position");
            header.add("snp");
            header.add("GT");
            header.add("GT1-2");
            //header.add("GT3");
            header.add("pA");
            header.add("pB");
            header.add("pH");
            header.add("nbReadsA");
            header.add("nbReadsB");
//            header.add("pA");
//            header.add("pB");
//            header.add("pH");
            writer.write(header.toString());
            writer.newLine();


            ArrayList<Snp> snpData = data.getSnpData();
            Probas proba = data.getProbas();
            int sampleIndex = data.getSamplesNames().indexOf(sampleName);
            System.out.println("sample index = " + sampleIndex);
            for (int v = 0; v < snpData.size(); v++) {           
                StringJoiner line = new StringJoiner(sep);
                line.add(this.chr);
                line.add(snpData.get(v).getPosition().toString());
                line.add(snpData.get(v).getName());
                if (ABHformat) {
                    String[] snpInABHBfImp = Functions.convertBytesToABH(data.getSnpDataBeforeImputation().get(v).getGenotypes());
                    line.add(snpInABHBfImp[sampleIndex]);
                    String[] snpInABH = Functions.convertBytesToABH(data.getSnpData().get(v).getGenotypes());
                    line.add(snpInABH[sampleIndex]);                     
                } else {
                    line.add(String.valueOf(data.getSnpDataBeforeImputation().get(v).getGenotypes()[sampleIndex]));
                    line.add(String.valueOf(data.getSnpData().get(v).getGenotypes()[sampleIndex]));
                }
                line.add(String.valueOf(proba.getpA()[sampleIndex][v]));
                line.add(String.valueOf(proba.getpB()[sampleIndex][v]));
                line.add(String.valueOf(proba.getpH()[sampleIndex][v]));
                line.add(String.valueOf(data.getSnpData().get(v).getNbReadsA()[sampleIndex]));
                line.add(String.valueOf(data.getSnpData().get(v).getNbReadsB()[sampleIndex]));
//                    line.add(String.valueOf(data.get(chr).getProbas().getpA()[0][v]));
//                    line.add(String.valueOf(data.get(chr).getProbas().getpB()[0][v]));
//                    line.add(String.valueOf(data.get(chr).getProbas().getpH()[0][v]));

                writer.write(line.toString());
                writer.newLine();
            }
            
            writer.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
    //FOR TESTING PURPOSES
    /**
     * Write zones A, B, H into a csv file (used in imputation step 3)
     * @param outputFilePath 
     */
    public void writeZones(String outputFilePath) {
        
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(outputFilePath))) {
            String sep = ",";
            
            StringJoiner header = new StringJoiner(sep);
            header.add("sample");
            header.add("chr");
            header.add("zone");
            header.add("start");
            header.add("stop");
            writer.write(header.toString());
            writer.newLine();            

            for (int s = 0; s < this.sampleNames.size(); s++) {
                List<Zone> zones = data.getABHzones().get(s);
                for (Zone z:zones) {
                    StringJoiner line = new StringJoiner(sep);
                    line.add(this.sampleNames.get(s));
                    line.add(this.chr);

                    switch (z.getGenotype()) {
                        case genPA1:
                            line.add("A");;
                            break;
                        case genPA2:
                            line.add("B");
                            break;
                        case genHTZ:
                            line.add("H");
                            break;
                        case genMIS:
                            line.add(".");
                    }

                    line.add(String.valueOf(z.getStart()));
                    line.add(String.valueOf(z.getEnd()));

                    writer.write(line.toString());
                    writer.newLine();

                }

            }
            
            writer.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /** 
     * Write a CSV file with the improbable chunks that were detected
     * @param outputFilePath 
     */
    public void writeCorrectedChunksFile(String outputFilePath) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(outputFilePath))) {
            String sep = ",";
            
            StringJoiner header = new StringJoiner(sep);
            header.add("sample");
            header.add("chr");
            header.add("startChunk");
            header.add("startPosition");
            header.add("endChunk");
            header.add("endPosition");
            header.add("corrected?");
            header.add("previousAllele");
            header.add("newAllele");
            header.add("dDB");
            header.add("dBE");
            header.add("rDBE");
            header.add("D");
            header.add("A");
            header.add("B");
            header.add("C");
            header.add("E");
            
            writer.write(header.toString());
            writer.newLine();            

            for (int sampleIndex : data.getChunks().keySet()) {
                String sample = sampleNames.get(sampleIndex);
                for (Chunk chunk:data.getChunks().get(sampleIndex)) {
                    StringJoiner line = new StringJoiner(sep);
                    line.add(sample);
                    line.add(this.chr);
                    line.add(String.valueOf(chunk.getStart()));
                    line.add(data.getSnpData().get(chunk.getStart()).getPosition().toString());
                    line.add(String.valueOf(chunk.getEnd()));
                    line.add(data.getSnpData().get(chunk.getEnd()).getPosition().toString());
                    String corrected = chunk.isCorrected() ? "yes" : "no";
                    line.add(corrected);

                    switch (chunk.getGenotype()) {
                        case genPA1:
                            line.add("A");;
                            break;
                        case genPA2:
                            line.add("B");
                            break;
                        case genHTZ:
                            line.add("H");
                            break;
                        case genMIS:
                            line.add(".");
                    } 
                    switch (chunk.getNewGenotype()) {
                        case genPA1:
                            line.add("A");;
                            break;
                        case genPA2:
                            line.add("B");
                            break;
                        case genHTZ:
                            line.add("H");
                            break;
                        case genMIS:
                            line.add(".");
                    } 
                    line.add(String.valueOf(chunk.getdDB()));
                    line.add(String.valueOf(chunk.getdBE()));
                    line.add(String.valueOf(chunk.getrDBE()));
                    line.add(String.valueOf(chunk.getD()));
                    line.add(String.valueOf(chunk.getA()));
                    line.add(String.valueOf(chunk.getB()));
                    line.add(String.valueOf(chunk.getC()));
                    line.add(String.valueOf(chunk.getE()));

                    writer.write(line.toString());
                    writer.newLine();
                }
            }            
            writer.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
    // FOR TESTING PURPOSE
    /** 
     * Write a csv file with allele counts for each snp
     * @param outputFilePath
     * @throws IOException 
     */
    public void writeSnpAlleleCounts(String outputFilePath) throws IOException {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(outputFilePath))) {
            String sep = ",";
        
            StringJoiner header = new StringJoiner(sep);
            header.add("chr");
            header.add("snp");
            header.add("nbA");
            header.add("nbB");
            header.add("nbH");
            header.add("nbMD");
            header.add("nbTotal");            

            for (Snp snp : data.getSnpData()) {
                StringJoiner line = new StringJoiner(sep);                
                line.add(this.chr);
                line.add(snp.getPosition().toString());
                line.add(String.valueOf(snp.getAlleleCounts().getNbA()));
                line.add(String.valueOf(snp.getAlleleCounts().getNbB()));
                line.add(String.valueOf(snp.getAlleleCounts().getNbH()));
                line.add(String.valueOf(snp.getAlleleCounts().getNbMD()));
                line.add(String.valueOf(snp.getAlleleCounts().getTotalCount()));
                writer.write(line.toString());
                writer.newLine();
            }
            
            writer.close();
        }
    }
    
    /**
     * Write a CSV file with SNPs that were not kept at the reading VCF step
     * @param excludedLines
     * @param outputFilePath
     * @throws IOException 
     */
    public static void writeExcludedLines(List<ExcludedLine> excludedLines, String outputFilePath) throws IOException {  
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(outputFilePath))) {
            String sep = ",";
            
            StringJoiner header = new StringJoiner(sep);
            header.add("chr");
            header.add("position");
            header.add("reason");
            header.add("parent1");
            header.add("parent2");
            writer.write(header.toString());
            writer.newLine();
            
            for (ExcludedLine exLine:excludedLines) {                
                
                StringJoiner line = new StringJoiner(sep);
                line.add(exLine.getChr());
                line.add(String.valueOf(exLine.getPosition()));
                line.add(exLine.getReason().toString());
                line.add(exLine.getParent1Allele().toString());
                line.add(exLine.getParent2Allele().toString()); 
                writer.write(line.toString());
                writer.newLine();
            }
        
            writer.close();
        } catch (Exception e) {
            System.out.println("Error writing breakpoints file");
            if (e.getMessage() != null) {
                System.out.println(e.getMessage());
            }
        }
    }
    
    public void writeFilteredDPOnReadsPerSample(String outputFilePath, List<int[]> filteredDP) throws IOException {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(outputFilePath + ".csv"))) {
            String sep = ",";
            
            StringJoiner header = new StringJoiner(sep);
            header.add("sample");
            header.add("filteredDP");
            header.add("totalDP");
            header.add("totalSnps");
            writer.write(header.toString());
            writer.newLine();            

            for (int s = 0; s < filteredDP.size(); s++) {                    
                StringJoiner line = new StringJoiner(sep);
                line.add(sampleNames.get(s));
                line.add(String.valueOf(filteredDP.get(s)[0]));
                line.add(String.valueOf(filteredDP.get(s)[1]));
                line.add(String.valueOf(filteredDP.get(s)[2]));
                writer.write(line.toString());
                writer.newLine();
            }
            
            writer.close();
        } catch (Exception e) {
            System.out.println("Error writing output file");
            System.out.println(e.getMessage());
        }
    }
}
