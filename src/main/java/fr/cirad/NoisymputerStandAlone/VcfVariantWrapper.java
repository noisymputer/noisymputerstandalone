/*******************************************************************************
 * NOISYmputer
 * Distributed under the GNU Affero General Public License V3.0
 * See <https://www.gnu.org/licenses/agpl-3.0.en.html> for details
 ******************************************************************************/
package fr.cirad.NoisymputerStandAlone;

import htsjdk.variant.variantcontext.VariantContext;
import java.util.ArrayList;
import java.util.List;
import noisymputer.VariantWrapper;

/**
 *
 * @author boizet
 */
public class VcfVariantWrapper implements VariantWrapper {
    VariantContext vc;

    public VcfVariantWrapper(VariantContext vc) {
        this.vc = vc;
    }   

    @Override
    public String getGenotype(String individual) {
        return vc.getGenotype(individual).getGenotypeString();
    }
    
    @Override
    public List<String> getSampleNames() {
        return new ArrayList(vc.getSampleNamesOrderedByName());
    }
    
    @Override
    public int getNSamples() {
        return vc.getNSamples();
    }

    @Override
    public int getDP(String individual) {
        return (int) vc.getGenotype(individual).getDP();
    }

    @Override
    public String getName() {
        if (vc.getID() != null) {
            return vc.getID();
        } else {
            return vc.getContig() + "_" + vc.getStart();
        }
        
    }

    @Override
    public long getPosition() {
        return vc.getStart();
    }
    
}
