/*******************************************************************************
 * NOISYmputer
 * Distributed under the GNU Affero General Public License V3.0
 * See <https://www.gnu.org/licenses/agpl-3.0.en.html> for details
 ******************************************************************************/
package fr.cirad.NoisymputerStandAlone;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.hash.HashCode;
import com.google.common.hash.Hashing;
import fr.cirad.NoisymputerStandAlone.ExcludedLine.Reason;
import htsjdk.tribble.AbstractFeatureReader;
import htsjdk.tribble.FeatureReader;
import htsjdk.variant.variantcontext.Allele;
import htsjdk.variant.variantcontext.Genotype;
import htsjdk.variant.variantcontext.VariantContext;
import htsjdk.variant.vcf.VCFCodec;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;
import noisymputer.AllDataInBytes;
import static noisymputer.AllDataInBytes.genHTZ;
import static noisymputer.AllDataInBytes.genMIS;
import static noisymputer.AllDataInBytes.genPA1;
import static noisymputer.AllDataInBytes.genPA2;
import noisymputer.AlleleCounts;
import noisymputer.NoisymputerReader;
import noisymputer.RefAlleles;
import noisymputer.Snp;
import noisymputer.SnpSelectionCriteria;

/**
 *
 * @author boizet
 */
public class VcfNoisymputerReader implements NoisymputerReader {
    private NoisymputerProperties conf;
    private Set<String> chrInFile;
    private String rawDataDirName;
    private static Pattern PARENT_PATTERN = Pattern.compile("(?i)^parent"); //a sample is a parent if it starts with "parent" (case insensitive)
    
    public VcfNoisymputerReader(NoisymputerProperties conf, String rawDataDirName) {
        this.conf = conf;
        this.rawDataDirName = rawDataDirName;
    }

    public Set<String> getChrInFile() {
        return chrInFile;
    }

    public void setChrInFile(Set<String> chrInFile) {
        this.chrInFile = chrInFile;
    }
    
    /**
     * Read the input VCF file to extract for each snp and each individual the genotypes and the A and B reads number
     * The SNPs for which at least one parent genotype is missing are not kept
     * The SNPs for which at least one parent is not homozygous are not kept
     * The SNPs for which parents genotypes are identical are not kept
     * Get the size of every chromosome (nb of snps)
     * @return the biggest chromosome (maximum number of snps)
     * @throws IOException
     * @throws Exception 
     */
    @Override
    public String getSnps() throws IOException, Exception {
        SnpSelectionCriteria ssc = conf.getCriterias();

        VCFCodec vc = new VCFCodec();
        if (!new File(conf.getFilePath()).isFile()) {
            throw new NoSuchFileException(conf.getFilePath());
        }        

        FeatureReader<VariantContext> reader = AbstractFeatureReader.getFeatureReader(conf.getFilePath(), vc, false);
        Iterator<VariantContext> variantIterator = reader.iterator();

        this.chrInFile = new HashSet<>();
        Map<String, AllDataInBytes> variants = new LinkedHashMap<>();
        long readLinesNb = 0;
        long totalSnpsNb = 0;
        long keptSnpsNb = 0;
        
        List<ExcludedLine> excludedLines = new ArrayList();
        int missingParentGTNbs = 0;
        int nonHMZParentGTNbs = 0;
        int identicalParentsGTNbs = 0;
        String chr = "";
        AllDataInBytes chrData = new AllDataInBytes();
        int snpNbMax = 0;
        String biggestChr = null;
        
        
    
        while (variantIterator.hasNext()) {
            VariantContext vcfEntry = variantIterator.next();
            
            readLinesNb++;
            if (readLinesNb%50000 == 0) {
                System.out.println("Reading " + readLinesNb + " lines");
            }
            
            if (!vcfEntry.getContig().equals(chr)) {
                //save previous chr data
                if (!chr.equals("")) {
                    if (chrData.getSnpData().size() > snpNbMax) {
                        biggestChr = chr;
                    }
                    storingRawData(new File(conf.getFilePath()), chr, chrData);
                }
                
                chr = vcfEntry.getContig();
                chrInFile.add(chr);
                chrData = new AllDataInBytes();
            }
            
            long position = vcfEntry.getStart();         
  
            totalSnpsNb++;

            if (chrData.getSamplesNames().isEmpty()) {
                chrData.setSamplesNames(new ArrayList(vcfEntry.getSampleNamesOrderedByName()));
                for (int s = 0; s < chrData.getSamplesNames().size(); s++) {
                    String sampleName = chrData.getSamplesNames().get(s);
                    if (chrData.getSamplesNames().equals(ssc.getParent1()) 
                            || chrData.getSamplesNames().equals(ssc.getParent2()) 
                            || PARENT_PATTERN.matcher(sampleName).find()) {
                        chrData.getParentIndexes().add(s);
                    }
                }
            }

            Allele reference = vcfEntry.getReference();
            Genotype parent1GT = vcfEntry.getGenotype(ssc.getParent1());
            Genotype parent2GT = vcfEntry.getGenotype(ssc.getParent2());
            if (parent1GT == null || parent2GT == null) {
                throw new Exception("wrong parents names");
            }

            if (parent1GT.isNoCall() || parent2GT.isNoCall()) {
                missingParentGTNbs++;
                excludedLines.add(new ExcludedLine(chr, vcfEntry.getStart(), Reason.MISSING_PARENT_GENOTYPE, parent1GT.getAlleles(), parent2GT.getAlleles()));
            } else if (!parent1GT.getAllele(0).equals(parent1GT.getAllele(1)) || !parent2GT.getAllele(0).equals(parent2GT.getAllele(1))) {
                nonHMZParentGTNbs++;
                excludedLines.add(new ExcludedLine(chr, vcfEntry.getStart(), Reason.NON_HMZ_PARENT, parent1GT.getAlleles(), parent2GT.getAlleles()));
            } else if (parent1GT.sameGenotype(parent2GT)) {
                identicalParentsGTNbs++;
                excludedLines.add(new ExcludedLine(chr, vcfEntry.getStart(), Reason.IDENTICAL_PARENT, parent1GT.getAlleles(), parent2GT.getAlleles()));
            } else { 
                keptSnpsNb++;
                byte[] snpInBytes =  new byte[vcfEntry.getSampleNames().size()];
                byte[] nbReadsA =  new byte[vcfEntry.getSampleNames().size()];
                byte[] nbReadsB =  new byte[vcfEntry.getSampleNames().size()];

                int nbA = 0, nbB = 0, nbH = 0, nbMD = 0;

                for (int i = 0; i < chrData.getSamplesNames().size(); i++) {      

                    String sample = chrData.getSamplesNames().get(i);
                    Genotype genotype = vcfEntry.getGenotype(sample);

                    if (genotype.getAD() == null) {
                        nbReadsA[i] = 0;
                        nbReadsB[i] = 0;
                    } else {
                        //127 is the upper limit of byte
                        if (parent1GT.getAllele(0).equals(reference)) {                            
                            nbReadsA[i] = (byte) (genotype.getAD()[0] > 127 ? 127 : genotype.getAD()[0]);
                            nbReadsB[i] = (byte) (genotype.getAD()[1] > 127 ? 127 : genotype.getAD()[1]);
                         } else {
                            nbReadsA[i] = (byte) (genotype.getAD()[1] > 127 ? 127 : genotype.getAD()[1]);
                            nbReadsB[i] = (byte) (genotype.getAD()[0] > 127 ? 127 : genotype.getAD()[0]);
                         }
                    }

                    if (genotype.sameGenotype(parent1GT)) {
                        snpInBytes[i] = genPA1;
                        nbA++;
                    } else if (genotype.sameGenotype(parent2GT)) {
                        snpInBytes[i] = genPA2;
                        nbB++;
                    } else if ( (genotype.getAllele(0).equals(parent1GT.getAllele(0)) && genotype.getAllele(1).equals(parent2GT.getAllele(1))) 
                            || (genotype.getAllele(0).equals(parent2GT.getAllele(0)) && genotype.getAllele(1).equals(parent1GT.getAllele(0))) ){ 
                        snpInBytes[i] = genHTZ;
                        nbH++;
                    } else {
                        snpInBytes[i] = genMIS;
                        nbMD++;
                    }
                }

                AlleleCounts alleleCounts = new AlleleCounts(nbA, nbB, nbH, nbMD);

                String snpName;
                if (vcfEntry.getID() != null && vcfEntry.getID() != "." ) {
                    snpName = vcfEntry.getID();
                } else {
                    snpName = vcfEntry.getContig() + "_" + vcfEntry.getStart();
                }

                String[] array = new String[vcfEntry.getAlleles().size()];
                for (int i=0; i<vcfEntry.getAlleles().size(); i++) {
                    array[i] = vcfEntry.getAlleles().get(i).getBaseString();
                }
                RefAlleles refAlleles = new RefAlleles(array, parent1GT.getAllele(0).getBaseString(), parent2GT.getAllele(0).getBaseString());

                Snp newSnp = new Snp(snpName, position, refAlleles, nbReadsA, nbReadsB, snpInBytes, alleleCounts);

                chrData.getSnpData().add(newSnp);
            }
        }
        
        //save last chr data
        storingRawData(new File(conf.getFilePath()), chr, chrData);
        if (biggestChr == null) {
            biggestChr = chr;
        }
 
        if (conf.getAllOutputs()) {
            WritingOutputs.writeExcludedLines(excludedLines, Paths.get(conf.getResultFilesDirectory(), "excludedLines.csv").toString());
        }
        System.out.println(missingParentGTNbs + " variants were excluded because at least one parent genotype was missing");
        System.out.println(nonHMZParentGTNbs + " variants were excluded because at least one parent was not homozygous");
        System.out.println(identicalParentsGTNbs + " variants were excluded because parents genotypes were identical");
        System.out.println(keptSnpsNb + " variants were kept on a total of " + totalSnpsNb + " variants");
        return biggestChr;
    }
    
    /**
     * After the csv has been read at the first run, the data are stored in a json file in the rawData directory
     * This json file is used on next runs in order to avoid reading the vcf file again
     * @param file the input VCF file
     * @param chr the chromosome
     * @param data the raw data of the chromosome
     * @throws IOException 
     */
    public void storingRawData(File file, String chr, AllDataInBytes data) throws IOException {
        if (!Files.exists(Paths.get(rawDataDirName))) {
            Files.createDirectory(Paths.get(rawDataDirName));
        }
        HashCode dirHash = com.google.common.io.Files.hash(file, Hashing.md5());
        Path dirPath = Paths.get(rawDataDirName, dirHash.toString());
        if (!Files.exists(dirPath)) {
            Files.createDirectory(dirPath);
        }

        String jsonPath = Paths.get(dirPath.toString(), chr + ".json").toString();
        File jsonFile = new File(jsonPath);
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonFile, data);
        System.out.println("Storing raw data into: " + jsonFile.getAbsolutePath());
    }
    
    //FOR TESTING PURPOSE
    /**
     * read and extract only genotypes from input VCF file
     * @return
     * @throws IOException
     * @throws Exception 
     */
    public Map<String, AllDataInBytes> getOnlyGenotypes() throws IOException, Exception {
        SnpSelectionCriteria ssc = conf.getCriterias();
        //Map<String, List<long[]>> map = ssc.getRegionsMap();
        
        VCFCodec vc = new VCFCodec();
        if (!new File(conf.getFilePath()).isFile()) {
            throw new NoSuchFileException(conf.getFilePath());
        }        

        FeatureReader<VariantContext> reader = AbstractFeatureReader.getFeatureReader(conf.getFilePath(), vc, false);
        Iterator<VariantContext> variantIterator = reader.iterator();
        //CloseableTribbleIterator iterator = reader.query(ssc.getChromosome(), null, null);
        List<String> chrList = reader.getSequenceNames();
        boolean query = reader.isQueryable();
        //CloseableTribbleIterator iterator = query(ssc.getChromosome());
        
        this.chrInFile = new HashSet<>();
        Map<String, AllDataInBytes> variants = new LinkedHashMap<>();
        long readLinesNb = 0;
        long totalSnpsNb = 0;
        long keptSnpsNb = 0;
        
        List<ExcludedLine> excludedLines = new ArrayList();
        int missingParentGTNbs = 0;
        int nonHMZParentGTNbs = 0;
        int identicalParentsGTNbs = 0;
        String chr = "";
        AllDataInBytes chrData = new AllDataInBytes();
        int snpNbMax = 0;
        String biggestChr = null;
        while (variantIterator.hasNext()) {
            VariantContext vcfEntry = variantIterator.next();
            
            readLinesNb++;
            if (readLinesNb%50000 == 0) {
                System.out.println("Reading " + readLinesNb + " lines");
            }
            
            if (!vcfEntry.getContig().equals(chr)) {
                //save previous chr data
                if (!chr.equals("")) {
                    variants.put(chr, chrData);
                    //storingRawData(new File(conf.getFilePath()), chr, chrData);
                }
                
                chr = vcfEntry.getContig();
                chrInFile.add(chr);
                chrData = new AllDataInBytes();
            }
            
            long position = vcfEntry.getStart();
  
            totalSnpsNb++;


            if (chrData.getSamplesNames().isEmpty()) {
                chrData.setSamplesNames(new ArrayList(vcfEntry.getSampleNamesOrderedByName()));
                for (int s = 0; s < chrData.getSamplesNames().size(); s++) {
                    String sampleName = chrData.getSamplesNames().get(s);
                    if (chrData.getSamplesNames().equals(ssc.getParent1()) 
                            || chrData.getSamplesNames().equals(ssc.getParent2()) 
                            || PARENT_PATTERN.matcher(sampleName).find()) {
                        chrData.getParentIndexes().add(s);
                    }
                }
            }

            Allele reference = vcfEntry.getReference();
            Genotype parent1GT = vcfEntry.getGenotype(ssc.getParent1());
            Genotype parent2GT = vcfEntry.getGenotype(ssc.getParent2());
            
            if (parent1GT == null || parent2GT == null) {
                throw new Exception("wrong parents names");
            }
            
            Allele p1 = parent1GT.getAllele(0);
            Allele p2 = parent2GT.getAllele(0);
            
            byte[] snpInBytes =  new byte[vcfEntry.getSampleNames().size()];

            for (int i = 0; i < chrData.getSamplesNames().size(); i++) {
                String sample = chrData.getSamplesNames().get(i);
                Genotype genotype = vcfEntry.getGenotype(sample);
                if (genotype.sameGenotype(parent1GT)) {
                    snpInBytes[i] = genPA1;
                } else if (genotype.sameGenotype(parent2GT)) {
                    snpInBytes[i] = genPA2;
                } else if ( (genotype.getAllele(0).equals(parent1GT.getAllele(0)) && genotype.getAllele(1).equals(parent2GT.getAllele(1))) 
                        || (genotype.getAllele(0).equals(parent2GT.getAllele(0)) && genotype.getAllele(1).equals(parent1GT.getAllele(0))) ){ 
                    snpInBytes[i] = genHTZ;
                } else {
                    snpInBytes[i] = genMIS;
                } 
            }

            String snpName;
            if (vcfEntry.getID() != null && vcfEntry.getID() != "." ) {
                snpName = vcfEntry.getID();
            } else {
                snpName = vcfEntry.getContig() + "_" + vcfEntry.getStart();
            }

            String[] array = new String[vcfEntry.getAlleles().size()];
            for (int i=0; i<vcfEntry.getAlleles().size(); i++) {
                array[i] = vcfEntry.getAlleles().get(i).getBaseString();
            }
            RefAlleles refAlleles = new RefAlleles(array, parent1GT.getAllele(0).getBaseString(), parent2GT.getAllele(0).getBaseString());

            Snp newSnp = new Snp(snpName, position, refAlleles, null, null, snpInBytes, null);

            chrData.getSnpData().add(newSnp);
        }       
        
        //save last chr data
        variants.put(chr, chrData);
        
        return variants;
        
    }  

}
