/*******************************************************************************
 * NOISYmputer
 * Distributed under the GNU Affero General Public License V3.0
 * See <https://www.gnu.org/licenses/agpl-3.0.en.html> for details
 ******************************************************************************/
package fr.cirad.NoisymputerStandAlone;

import htsjdk.variant.variantcontext.Allele;
import java.util.List;

/**
 * 
 * @author boizet
 */
public class ExcludedLine {
    private String chr;
    private long position;
    private Reason reason;
    public enum Reason {MISSING_PARENT_GENOTYPE, NON_HMZ_PARENT, IDENTICAL_PARENT};
    private List<Allele> parent1Allele;
    private List<Allele> parent2Allele;

    public ExcludedLine(String chr, long position, Reason reason, List<Allele> parent1Allele, List<Allele> parent2Allele) {
        this.chr = chr;
        this.position = position;
        this.reason = reason;
        this.parent1Allele = parent1Allele;
        this.parent2Allele = parent2Allele;
    }

    public String getChr() {
        return chr;
    }

    public void setChr(String chr) {
        this.chr = chr;
    }

    public long getPosition() {
        return position;
    }

    public void setPosition(long position) {
        this.position = position;
    }

    public Reason getReason() {
        return reason;
    }

    public void setReason(Reason reason) {
        this.reason = reason;
    }

    public List<Allele> getParent1Allele() {
        return parent1Allele;
    }

    public void setParent1Allele(List<Allele> parent1Allele) {
        this.parent1Allele = parent1Allele;
    }

    public List<Allele> getParent2Allele() {
        return parent2Allele;
    }

    public void setParent2Allele(List<Allele> parent2Allele) {
        this.parent2Allele = parent2Allele;
    }

    
}
