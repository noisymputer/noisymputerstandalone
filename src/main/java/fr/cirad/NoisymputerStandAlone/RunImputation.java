/*******************************************************************************
 * NOISYmputer
 * Distributed under the GNU Affero General Public License V3.0
 * See <https://www.gnu.org/licenses/agpl-3.0.en.html> for details
 ******************************************************************************/
package fr.cirad.NoisymputerStandAlone;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.hash.HashCode;
import com.google.common.hash.Hashing;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import static java.lang.Math.round;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import noisymputer.AllDataInBytes;
import noisymputer.Exceptions.BreakpointWindowException;
import noisymputer.Exceptions.EmptySnpListException;
import noisymputer.SnpSelectionCriteria;
import static noisymputer.SnpSelectionCriteria.PopType.DH;
import static noisymputer.SnpSelectionCriteria.PopType.SSD;
import noisymputer.Exceptions.WindowSizeException;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 *
 * @author boizet
 */
@Component
public class RunImputation implements CommandLineRunner {
    
    @Autowired
    NoisymputerProperties conf;   
    
    final String rawDataDirName = "rawData";
    
    @Override
    public void run(String... args) throws Exception {
        
        System.out.println("");
        System.out.println("### RUNNING NOISYMPUTER ###");
        System.out.println("");
        
        final long startTime = System.currentTimeMillis();
        
        //check parameters
        SnpSelectionCriteria criterias =  conf.getCriterias();
        ArrayList<String> missingParameters = new ArrayList();
       
        if (criterias.getParent1() == null) {
            missingParameters.add("parent1");
        }
        if (criterias.getParent2() == null) {
            missingParameters.add("parent2");
        }
        
        if (!missingParameters.isEmpty()) {
            System.out.println("Some parameters are missing " + missingParameters.toString());
            return;
        }
        
        boolean keepIntermediateResults = conf.isKeepIntermediateResults();
        boolean allOutputs = conf.getAllOutputs();
        
        if (conf.getResultFilesDirectory() == null || StringUtils.isEmpty(conf.getResultFilesDirectory())) {            
            conf.setResultFilesDirectory(Paths.get("NoisymputerResults").toAbsolutePath().toString());
            if (!Files.exists(Paths.get("NoisymputerResults"))) {
                Files.createDirectory(Paths.get("NoisymputerResults"));
            }
        } else {
            if (!Files.exists(Paths.get(conf.getResultFilesDirectory()))) {
                System.out.println("ERROR - The specified Results directory doesn't exist, please check resultFilesDirectory parameter");
                return;
            }
        }
        

//        AllDataInBytes data = new AllDataInBytes();
//        data.readMapFile();
//        data.detectAlienSegments2("chr09", criterias);
        
        if (conf.getFilePath().isEmpty()) {
            File curDir = new File(".");
            //find .vcf in current directory
            FilenameFilter csvFilter = (dir, ext) -> ext.endsWith(".vcf") || ext.endsWith(".vcf.gz");
            
            String[] fileNames = curDir.list(csvFilter);
            
            System.out.println(Arrays.asList(fileNames).toString());
            if (fileNames.length == 0) {
                System.out.println("ERROR - No vcf file was found in current directory");
                return;
            } else if (fileNames.length > 1) {
                System.out.println("ERROR - Several VCF file were found in current directory, please specify in filePath the name of the file");
                return;
            } else {
                conf.setFilePath(fileNames[0]);
            }
        }
        
        File file = new File(conf.getFilePath());
        
        System.out.println("File to read: " + file.getAbsolutePath());
        if (!Files.exists(file.toPath())) {
            System.out.println("ERROR - The vcf file doesn't exist");
            return;
        }
        
        Path filesDirectory = Paths.get(conf.getResultFilesDirectory());        
        
        Map<String, AllDataInBytes> variantsMap = null;
        
        if (conf.isCleanRawDataDirectory()) {
            cleanRawDataDirectory();
        }    
        
        //FOR TESTING PURPOSE
        //read imputed vcf file and generate Breakpoints file
        //used to extract breakpoints from VCF file generated by another tool 
        if (conf.isExtractBreakpoints()) {
            try {
                VcfNoisymputerReader vcfReader = new VcfNoisymputerReader(conf, this.rawDataDirName);  
                Map<String, AllDataInBytes> genotypes = vcfReader.getOnlyGenotypes();
                
                for (String chr : genotypes.keySet()) {
                    genotypes.get(chr).extractBreakpoints(chr);
                    Integer bpNb = genotypes.get(chr).getBkps().values().stream().mapToInt(List::size).sum();
                    System.out.println(bpNb + " breakpoints were found (~" + bpNb/genotypes.get(chr).getSamplesNames().size() + " breakpoints per individual)");
                    if (bpNb > 0) {
                        WritingOutputs outputs = new WritingOutputs(genotypes.get(chr), chr);
                        final String pathFileName = Paths.get(filesDirectory.toString(), chr).toString();
                        outputs.writeBreakPoints(pathFileName + "_BreakpointsFromVCF.csv");
                        System.out.println("Breakpoints saved in " + pathFileName + "_BreakpointsFromVCF.csv file");
                    }
                                       
                }
                return;                
            } catch (Exception e) {
                System.out.println(e.getMessage());
                System.out.println("ERROR - can't read vcf file");
                return;
            }
        }        
        
        //try reading stored raw data
        HashCode dirHash = com.google.common.io.Files.hash(file, Hashing.md5());
        boolean exists = Files.exists(Paths.get(this.rawDataDirName, dirHash.toString())); 
        File directory = new File(Paths.get(this.rawDataDirName, dirHash.toString()).toString());
        System.out.println("Trying to find rawData directory : " + directory.getAbsolutePath());
        
        Map<String, List<long[]>> regionsMap = criterias.getRegionsMap();
        Map<String, List<long[]>> excludedRegionsMap = criterias.getExcludedRegionsMap();
        
        boolean hasLocalData = false;
        if (conf.isReadRawDataFromStoringFile() && exists) {
            String[] filesArray;
            if (!criterias.getRegions().isEmpty()) {
                filesArray = new String[regionsMap.size()];
                int i = 0;
                for (String chr:regionsMap.keySet()) {
                    filesArray[i] = chr + ".json";
                    i++;
                }
            } else {
                filesArray = directory.list();
            }
            
            //reading data from local stored file            
            ObjectMapper objectMapper = new ObjectMapper();
           
            variantsMap = new HashMap<>();        
            
            for (int i = 0; i < filesArray.length; i++) {
                if (filesArray[i].endsWith("json")) {
                    String chr = filesArray[i].replace(".json", "");
                    if (Files.exists(Paths.get(directory.getPath(), filesArray[i]))) { 
                        hasLocalData = true;                    
                    } else {
                        System.out.println("No local stored file was found for " + chr);
                        break;
                    }
                }
            }
            
        } else {
            System.out.println("No local stored file was found");
        }
        
        //String[] filesArray = null;
        List<String> filesList = new ArrayList();
        boolean initFinalFile = true;
        if (!hasLocalData) {
            //1. Read VCF file and convert genotypes in bytes and calculate frequencies
            System.out.println("");
            System.out.println("# READING VCF FILE AND CONVERTING SNPS IN BYTES #");
            try {
                VcfNoisymputerReader vcfReader = new VcfNoisymputerReader(conf, this.rawDataDirName);  
                String biggestChr = vcfReader.getSnps();
                File jsonFile = new File(Paths.get(directory.getPath(), biggestChr + ".json").toString());
                ObjectMapper objectMapper = new ObjectMapper();
                AllDataInBytes biggestChrData = objectMapper.readValue(jsonFile, AllDataInBytes.class);
                biggestChrData.filterSnpsOnGenotypicFrequencies(criterias, false);
                int biggestChrSize = biggestChrData.getSnpData().size();
                BufferedWriter writer = new BufferedWriter(new FileWriter(new File(Paths.get(directory.getPath(), "biggestChrSize.txt").toString())));
                writer.write(String.valueOf(biggestChrSize));
                writer.close();
                
//                if (variantsMap.isEmpty()) {
//                    System.out.println("No snps were found, please check parameter param.criterias.regions, must be one of these: " + vcfReader.getChrInFile().toString().replace("[", "").replace("]",""));
//                    return;
//                }
            } catch (NoSuchFileException e) {
                System.out.println("File " + file.getAbsolutePath() + " not found");
                return;
            } catch (Exception e) {
                System.out.println("An error occured: " + e.getMessage());
                return;
            }                
            
        } 
        if (!criterias.getRegions().isEmpty()) {
            for (String chr:regionsMap.keySet()) {
                filesList.add(chr + ".json");
            }
        } else {
            File[] files = directory.listFiles();
            //to keep order of chromosomes from vcf file
            Arrays.sort(files, Comparator.comparingLong(File::lastModified));
            for (int i = 0; i < files.length; i++) {
                if (files[i].getName().endsWith("json")) {
                    filesList.add(files[i].getName());
                }
            }

        }
        
        System.out.println("");
        
        //get incoherentHalfWindowSize
        if (criterias.getIncoherentHalfWindowSize() == -1) {
            File biggestChrFile = new File(Paths.get(directory.getPath(), "biggestChrSize.txt").toString());
            int snpNb = Integer.parseInt(FileUtils.readFileToString(biggestChrFile, "UTF-8"));
            criterias.setIncoherentHalfWindowSize((int) round(0.01 * snpNb));            
        }
        
        if (criterias.getBreakpointHalfWindowSize() == -1) { //not defined by user
            criterias.setBreakpointHalfWindowSize(round(criterias.getImputeHalfWindowSize() / 2));
        }

        writingParamFile(filesDirectory);      
        
        ObjectMapper objectMapper = new ObjectMapper();
        
        if (!filesList.isEmpty()) {
            for (int i = 0; i < filesList.size(); i++) {
                File jsonFile = new File(Paths.get(directory.getPath(), filesList.get(i)).toString());
                String chr = jsonFile.getName().replace(".json", "");
                AllDataInBytes chrData;
                try { 
                    System.out.println("__________________________________________________________________");

                    System.out.println("");
                    System.out.println("# WORKING ON CHROMOSOME: " + chr);
                    System.out.println("");
                    System.out.println("Reading file: " + jsonFile.getAbsolutePath());
                    chrData = objectMapper.readValue(jsonFile, AllDataInBytes.class);
                } catch (Exception e) {
                    System.out.println("An error occured while reading local stored file : " + jsonFile.getName());
                    continue;
                }
                chrData.setErrorA(criterias.getErrorA());
                chrData.setErrorB(criterias.getErrorB());
               
                final String pathFileName = Paths.get(filesDirectory.toString(), chr).toString();          
                int stepNumber = 0;
                String fileName = pathFileName + "_" + stepNumber + "GT";    

                System.out.println("Writing raw data output: " + fileName + "." + conf.getExportFormat());

                WritingOutputs outputs = new WritingOutputs(chrData, chr);

                System.out.println(chrData.getSnpData().size() + " SNPs");

                if (conf.isStopBeforePreImputation()) {
                    return;
                }

                System.out.println("");
                System.out.println("## PRE-IMPUTATION FILTERING ##");
                System.out.println("");
                
                if ((regionsMap.get(chr) != null && !regionsMap.isEmpty()) 
                        || (excludedRegionsMap.get(chr) != null && !excludedRegionsMap.isEmpty())
                        ) {
                    System.out.println("--- STEP " + stepNumber + " - FILTERING ON REGIONS ---");
                    if (allOutputs) {
                        outputs.writeFile(fileName, conf.getExportFormat());
                    }
                    chrData.filterVariantsOnRegions(chr, regionsMap.get(chr), excludedRegionsMap.get(chr));
                    fileName = pathFileName + "_" + stepNumber;
                    if (keepIntermediateResults || allOutputs) {
                        outputs.writeFile(fileName + "_GTafterFilteringOnRegions", conf.getExportFormat());
                        outputs.writeFrequencies(fileName + "_SnpGenotypicFrequencies" + ".csv");
                    }
                    System.out.println(chrData.getSnpData().size() + " kept SNPs");
                } else {
                    if (keepIntermediateResults || allOutputs) {
                        outputs.writeFile(fileName, conf.getExportFormat());
                        outputs.writeFrequencies(fileName + "_SnpGenotypicFrequencies" + ".csv");
                    }
                }
                
                //1. FILTER SNPS OR DATA ON READS
                stepNumber++;
                System.out.println("");
                System.out.println("--- STEP " + stepNumber + " - FILTERING ON READS ---");

                if (criterias.getMinReadsVariant() != -1 || criterias.getMaxReadsVariant() != -1) { 
                    chrData.filterOnReadsSNP(criterias);
                }

                List<int[]> filteredDPPerSample = chrData.filterOnReadsDP(criterias);                
                    
                fileName = pathFileName + "_" + stepNumber;
                if (keepIntermediateResults || allOutputs) {
                    outputs.writeFile(fileName + "_GTafterFilteringOnReads", conf.getExportFormat());
                    outputs.writeFilteredDPOnReadsPerSample(fileName + "_nbFilteredDPonReadsPerSample", filteredDPPerSample);
                }

                //2. FILTER SNPS ON MIN AND MAX GENOTYPIC FREQUENCIES
                if (criterias.isFilterVariantsOnGenotypicFrequencies()) {
                    stepNumber++;
                    System.out.println("");
                    System.out.println("--- STEP " + stepNumber + " - FILTER SNPS ON MIN AND MAX GENOTYPIC FREQUENCIES ---");

                    try {
                        chrData.filterSnpsOnGenotypicFrequencies(criterias, true);
                        fileName = pathFileName + "_" + stepNumber;
                        if (keepIntermediateResults  || allOutputs) {
                            outputs.writeFile(fileName  + "_GTafterFilteringSnpOut", conf.getExportFormat());
                        }
                    } catch (IOException | EmptySnpListException e) {
                        System.out.println(e.getMessage());
                        return;
                    }
                    
                }            

                //3. FILTERING OUT REDUNDANT LOCI
                if (criterias.isFilterOutRedundant()) {
                    stepNumber++;
                    System.out.println("");
                    System.out.println("--- STEP " + stepNumber + " - FILTER OUT REDUNDANT LOCI ---");
                    chrData.filterOutRedundant();
                    fileName = pathFileName + "_" + stepNumber;
                    if (keepIntermediateResults  || allOutputs) {
                        outputs.writeFile(fileName + "_GTafterFilteringRedundantSnpsOut", conf.getExportFormat());
                    }
                }

                //4. Correct false HTZ (only for SSD or DH population)
                if (criterias.isCorrectFalseHTZ() && (criterias.getPopType().equals(SSD) || criterias.getPopType().equals(DH))) {
                    stepNumber++;
                    try {
                        System.out.println("");
                        System.out.println("--- STEP " + stepNumber + " - CORRECT OBVIOUS ERRONEOUS HTZ CALLS ---");
                        chrData.correctErroneousHTZ();                          
                    } catch (WindowSizeException e) {
                        System.out.println(e.getMessage());
                        return;
                    }
                    fileName = pathFileName + "_" + stepNumber;
                    if (keepIntermediateResults  || allOutputs) {
                        outputs.writeFile(fileName + "_GTafterCorrectingFalseHTZ", conf.getExportFormat());
                    }
                }

                //5. Filter out incoherent snps (first filling missing data)
                if (criterias.isFilterOutIncoherent()) {
                    stepNumber++;
                    System.out.println("");
                    System.out.println("--- STEP " + stepNumber + " - DETECT INCOHERENT SNPS---");
                    if (!criterias.getPopType().equals(SnpSelectionCriteria.PopType.F2) && criterias.isFillMissingDataBefore()) {
                        chrData.fillingMissingData("");
                    }            
                    try {
                        chrData.detectIncoherentSNPs(criterias);
                    } catch (WindowSizeException e) {
                        System.out.println(e.getMessage());
                        return;
                    }
                    fileName = pathFileName + "_" + stepNumber;
                    if (keepIntermediateResults  || allOutputs) {
                        outputs.writeFile(fileName + "_GTafterFilteringIncoherentSnpsOut", conf.getExportFormat());
                    }
                }
                
                //save raw data
                chrData.saveSnpGenotypesBeforeImputation(); 

                //6. IMPUTATION
                if (criterias.isImpute()) {
                    stepNumber++;
                    System.out.println("");
                    System.out.println("## IMPUTATION ");

                    //IMPUTATION STEP 1 & 2 TO ESTIMATE ERRORS
                    if (criterias.isRecalculateErrorsBeforeStep3()) {
                        //Step 1 and 2 to estimate errors
                        System.out.print("Estimating errorA and errorB...\r");
                        chrData.imputeAllSamplesStep1(criterias); 
                        if (allOutputs) {
                            fileName = pathFileName + "_" + stepNumber + ".0.1";
                            outputs.writeFile(fileName + "_GTafterImputation", conf.getExportFormat());
                        }
                        if (criterias.isImputeStep2()) {                    
                            chrData.imputeAllSamplesStep2(criterias, chr);
                            if (allOutputs) {
                                fileName = pathFileName + "_" + stepNumber + ".0.2";
                                outputs.writeFile(fileName + "_GTafterImputation", conf.getExportFormat());
                            }
                        }                                
                        //Calculate errorA and errorB and launch again step1 and step2 with those errors   
                        chrData.calculateErrors(criterias);
                    }
                    chrData.copySnpGenotypes();

                    System.out.println("");
                    System.out.println("--- IMPUTATION - STEP " + stepNumber +".1 ---");
                    System.out.println("imputeHalfWindowSize = " + criterias.getImputeHalfWindowSize());
                    System.out.print("Imputing... \r");
                    
                    //IMPUTATION STEP 1 
                    chrData.imputeAllSamplesStep1(criterias);
                    if (keepIntermediateResults || allOutputs) {
                        fileName = pathFileName + "_" + stepNumber + ".1";                
                        outputs.writeFile(fileName + "_GTafterImputation", conf.getExportFormat());
                    }

                    //IMPUTATION STEP 2
                    if (criterias.isImputeStep2()) {
                        System.out.println("                ");
                        System.out.println("--- IMPUTATION - STEP " + stepNumber +".2 ---");
                        System.out.println("Filling missing data");
                        chrData.imputeAllSamplesStep2(criterias, chr);
                        if (keepIntermediateResults || allOutputs) {
                            fileName = pathFileName + "_" + stepNumber + ".2";
                            outputs.writeFile(fileName + "_GTafterImputation", conf.getExportFormat());
                        }
                    }
                    
                    //IMPUTATION STEP 3
                    try {
                        System.out.println("                ");
                        System.out.println("--- IMPUTATION - STEP " + stepNumber +".3 ---");                        
                        System.out.println("breakpointHalfWindowSize = " + criterias.getBreakpointHalfWindowSize());
                        System.out.print("Imputing... \r");
                        chrData.imputeAllSamplesStep3(criterias, chr, false);
                    } catch (BreakpointWindowException e) {
                        System.out.println("Error - " + e.getMessage());
                        System.out.println("Please retry with a smaller BreakpointHalfWindowSize");
                        return;
                    }

                    if (keepIntermediateResults || allOutputs) {
                        fileName = pathFileName + "_" + stepNumber + ".3";
                        outputs.writeFile(fileName + "_GTafterImputation", conf.getExportFormat());
                    }

                    Integer bpNb = chrData.getBkps().values().stream().mapToInt(List::size).sum();
                    System.out.println(bpNb + " breakpoints were found (~" + bpNb/chrData.getSamplesNames().size() + " breakpoints per individual)");
                    if (keepIntermediateResults || allOutputs) {
                        if (bpNb > 0) {
                            outputs.writeBreakPoints(fileName + "_Breakpoints.csv");
                        }
                    }
                }

                System.out.println("");
                System.out.println("## POST-IMPUTATION FILTERING ##");

                //7 DETECT IMPROBABLE SMALL CHUNKS
                if (criterias.isDetectImprobableChunks()) {
                    stepNumber++;
                    System.out.println("");
                    System.out.println("--- STEP " + stepNumber + " - DETECT IMPROBABLE CHUNKS---");

                    chrData.detectImprobableChunks(chr, criterias);
                    fileName = pathFileName + "_" + stepNumber;
                    if (keepIntermediateResults || allOutputs) {
                        outputs.writeCorrectedChunksFile(fileName + "_Chunks.csv");
                        outputs.writeFile(fileName + "_GTafterRemovingImprobableChunks", conf.getExportFormat());
                        Integer bpNb = chrData.getBkps().values().stream().mapToInt(List::size).sum();
                        if (bpNb > 0) {
                            outputs.writeBreakPoints(fileName + "_Breakpoints.csv");
                        }
                    }
                }

                if (allOutputs) {
                    //writing imputed snps frequencies
                    outputs.writeFrequencies(fileName + "_SnpGenotypicFrequencies.csv");
                    //write samples frequencies
                    outputs.writeSampleStats(fileName + "_SampleStats.csv");
                }

                //8 FILTER ALIEN SEGMENTS
                if (criterias.isFilterOutAlienSegments()) {
                    stepNumber++;
                    System.out.println("");
                    System.out.println("--- STEP " + stepNumber + " - REMOVING ALIEN SEGMENTS ---");

                    //8 COLLAPSING MATRIX BEFORE DETECTING ALIENS
                    System.out.print(stepNumber + ".1 - Collapsing matrix \r");
                    chrData.collapse(criterias.isKeepBothBreakpointMarkers());
                    System.out.println(stepNumber + ".1 - Collapsing matrix: " + chrData.getSnpDataCollapsed().size() + " kept SNPs");

                    if (keepIntermediateResults || allOutputs) {
                        fileName = pathFileName + "_" + stepNumber;
                        outputs.writeFile(fileName + "_GTafterCollapsing", conf.getExportFormat(), true);   
                    }                

                    System.out.print(stepNumber + ".2 - Detecting aliens... \r");
                    chrData.calculateMapOnCollapsing(chr, criterias, true);     
                    if (keepIntermediateResults || allOutputs) {
                        outputs.writeMareyMapToCSV(fileName + "_map.csv", criterias, true, true);
                    }

                    if (criterias.isNewAlienMethod()) {
                        System.out.println("using new method");
                    } else {
                        System.out.println("using current method");
                    }
                    int nbAliens = criterias.isNewAlienMethod() ? chrData.detectAlienSegments2(chr, criterias) : chrData.detectAlienSegments(chr, criterias);    
                    System.out.println(stepNumber + ".2 - Detect aliens: " + nbAliens + " found alien segments");

                    fileName = pathFileName + "_" + stepNumber;               


                    if (nbAliens > 0) {

                        //REMOVE ALIENS FROM RAW DATA
                        System.out.println(stepNumber + ".3 - Remove aliens from raw data");
                        int nbSnpsBefore = chrData.getSnpDataBeforeImputation().size();
                        chrData.removeAliens();
                        chrData.copySnpGenotypes();

                        if (keepIntermediateResults || allOutputs) {
                            outputs.writeAliens(fileName + "_Aliens.csv");
                            outputs.writeFile(fileName + "_GTafterRemovingAliens", conf.getExportFormat());
                        }


                        System.out.println(chrData.getSnpDataBeforeImputation().size() + " / " + nbSnpsBefore + " kept SNPs");


                        //IMPUTATION & CHUNKS REMOVAL ON RAW DATA WITH NO ALIEN
                        System.out.println(stepNumber + ".4 - Impute data with no aliens");

                        chrData.imputeAllSamplesStep1(criterias);
                        chrData.imputeAllSamplesStep2(criterias, chr);
                        chrData.imputeAllSamplesStep3(criterias, chr, false);

                        Integer bpNb = chrData.getBkps().values().stream().mapToInt(List::size).sum();
                        System.out.println(bpNb + " breakpoints were found (~" + bpNb/chrData.getSamplesNames().size() + " breakpoints per individual)");
//                        if (bpNb > 0) {
//                            outputs.writeBreakPoints(fileName + "_06.3_Breakpoints.csv");
//                        }

                        if (criterias.isDetectImprobableChunks()) {
                            System.out.println(stepNumber + ".5 - remove chunks from imputed data");
                            chrData.detectImprobableChunks(chr, criterias);
                        }
                    }                
                }

                //WRITING FINAL OUTPUTS
                fileName = pathFileName;
                if (keepIntermediateResults || allOutputs) {
                    fileName = pathFileName + "_final";
                }
                Integer bpNb = chrData.getBkps().values().stream().mapToInt(List::size).sum();
                if (bpNb > 0) {
                    outputs.writeBreakPoints(fileName + "_Breakpoints.csv");
                }
                if (criterias.isCollapseGenoMatrix()) {
                    chrData.collapse(criterias.isKeepBothBreakpointMarkers());
                    outputs.writeFile(fileName + "_GTafterCollapsing", conf.getExportFormat(), true);
                    chrData.calculateMapOnCollapsing(chr, criterias, true); 
                    outputs.writeMareyMapToCSV(fileName + "_mapCollapsed.csv", criterias, true, true);
                } else {
                    outputs.writeFile(fileName + "_GT", conf.getExportFormat(), false);
                }
                
                //writing statistics in console
                chrData.returnGenotypesSummaryStatistics(criterias);
                //writing imputed snps frequencies
                outputs.writeFrequencies(fileName + "_SnpGenotypicFrequencies.csv");
                //write samples frequencies
                outputs.writeSampleStats(fileName + "_SampleStats.csv");

                //writing genetic map
                System.out.println("");
                System.out.println("--- FINAL STEP - Writing final genetic linkage map ---");
                chrData.calculateMap(chr, criterias);
                chrData.updateAlleleCounts(true);
                outputs.writeMareyMapToCSV(fileName + "_map.csv", criterias);

                //TODO write final file with all chromosomes
                if (filesList.size() > 1) {
                    fileName = Paths.get(filesDirectory.toString(), "all_GT").toString(); 
                    if (initFinalFile) { //we create the file
                        outputs.writeFile(fileName, conf.getExportFormat(), criterias.isCollapseGenoMatrix());
                        initFinalFile = false;
                    } else { //we append new lines to the file 
                        outputs.writeFile(fileName, conf.getExportFormat(), criterias.isCollapseGenoMatrix(), true);
                    }
                }

            }              

            final long endTime = System.currentTimeMillis();
            long executionTime = endTime - startTime;

            System.out.println("execution time: " + String.format("%d min, %d sec", 
                TimeUnit.MILLISECONDS.toMinutes(executionTime),
                TimeUnit.MILLISECONDS.toSeconds(executionTime) - 
                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(executionTime))
            ));
        } else {
            System.out.println("no files to read");
        }

    }
    
    /**
     * Write actual used parameters into a json file
     * @param directoryPath the directory where the json file will be saved
     * @throws IOException 
     */
    public void writingParamJsonFile(Path directoryPath) throws IOException {
        Path outputFilePath = Paths.get(directoryPath.toString(), "parameters.json");
        System.out.println("Writing parameters file " + outputFilePath);
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(new File(outputFilePath.toString()), conf);
    }
    
    /**
     * Write actual used parameters into a txt file
     * @param directoryPath the directory where the file will be saved
     * @throws IOException 
     */
    public void writingParamFile(Path directoryPath) throws IOException {
        Path outputFilePath = Paths.get(directoryPath.toString(), "parameters.txt");
        System.out.println("writing parameters file " + outputFilePath);
        BufferedWriter writer = new BufferedWriter(new FileWriter(outputFilePath.toString()));
        
        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> map = mapper.convertValue(conf, Map.class);
        
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            String key = entry.getKey();
            if (!key.equals("criterias")) {
                Object value = entry.getValue();
                writer.write("param." + key + "=" + value.toString());
                writer.newLine();
            
            } else {
                Map<String, Object> criterias = (Map) map.get("criterias");            
                for (Map.Entry<String, Object> cr : criterias.entrySet()) {
                    key = cr.getKey();
                    if (!key.equals("regionsMap") && !key.equals("excludedRegionsMap")) {
                        Object value = cr.getValue();
                        String returnedValue = value.toString();
                        if (key.equals("regions")) {
                            List<String> regions = (List<String>) value;
                            if (regions.isEmpty()) {
                                continue;
                            } else {
                                returnedValue = String.join(",", regions);
                            }
                        }
                        if (key.equals("excludedRegions")) {
                            List<String> excludedRegions = (List<String>) value;
                            if (excludedRegions.isEmpty()) {
                                continue;
                            } else {
                                returnedValue = String.join(",", excludedRegions);
                            }
                        }
                        writer.write("param.criterias." + key + "=" + returnedValue);
                        writer.newLine();
                    }
                }
            }            
        }        
        writer.close();
    }

    /**
     * Remove the raw data directory
     * @throws IOException 
     */
    private void cleanRawDataDirectory() throws IOException {       
        if (Files.exists(Paths.get(this.rawDataDirName))) {
            File dir = new File(this.rawDataDirName);
            FileUtils.cleanDirectory(dir);
            System.out.println("Cleaning directory: " + dir.getAbsolutePath());
        }        
    }
    
    
}
