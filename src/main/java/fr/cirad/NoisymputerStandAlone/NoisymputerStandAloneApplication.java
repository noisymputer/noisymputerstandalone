/*******************************************************************************
 * NOISYmputer
 * Distributed under the GNU Affero General Public License V3.0
 * See <https://www.gnu.org/licenses/agpl-3.0.en.html> for details
 ******************************************************************************/
package fr.cirad.NoisymputerStandAlone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties(NoisymputerProperties.class)
public class NoisymputerStandAloneApplication {

    public static void main(String[] args) {
        // Tell Boot to look for my-project.properties instead of application.properties
        System.setProperty("spring.config.name", "NOISYmputer");
        SpringApplication.run(NoisymputerStandAloneApplication.class, args);

    }

}
