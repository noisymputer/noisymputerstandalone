# Parameters description

Parameters designated as \*Required MUST be modified by the user before launching a run of NOISYmputer.

## Inputs and outputs

| Parameter | Alias | Type | Default | Description |
|-----------|-------|------|---------|-------------|
| filePath | f | String | \*Required | Indicate the path (and name) of the input VCF file to impute. The vcf can be gziped. |
| resultFilesDirectory | o | String | NoisymputerResults | Indicate the path of the output folder where result files will be stored. If the parameter is commented, a sub-folder `NoisymputerResults` will be create in the current working directory. |
| exportFormat | e | String | CSV_BYTE | Type of outputs to be produced. Supported export format for imputed data are CSV_BYTE, CSV_ABH, HAPMAP or VCF. (For CSV_BYTE, 1 corresponds to A, 2 to H, 3 to B and 9 for missing data)|
| keepIntermediateResults | k | Boolean | false | Store intermediate results files at each step of the algorithm. |

## Crossing informations

| Parameter | Alias | Type | Default | Description |
|-----------|-------|------|---------|-------------|
| popType | p | String | F2 | Indicate the population type. Arguments taken are F2, BC1, SSD, DH and BCSSD. |
| parent1 | p1 | String | \*Required | Parent 1 ID exactly as mentionned in the input VCF. |
| parent2 | p2 | String | \*Required | Parent 2 ID exactly as mentionned in the input VCF. |

Prerequisite: All parents must have the prefix "Parent" in their names.
Several parental samples can be include in the input VCF file. However, they must all have the prefix "Parent" to exclude them from statistical analyses of the offspring.

## Pre-imputation filtering

| Parameter | type | Default | description |
|-----------|------|---------|-------------|
| regions | List<String> | *empty list* | Specify the chromosome name you wish to impute as written in the contig column of the input VCF (e.g., chr01). If this parameter is not given, then all chromosomes are imputed. If one would like to specify a narrowed region on a chromosome, it can be achieved using the following syntax: chromosome:start-stop (e.g., chr1:10000-30000, chr2:12300-45021, chr1:-120000, chr2:345000-).|
| excludedRegions | List<String> | *empty list* | Same as regions, you can specify which segments of chromosome you want to exclude before imputing |
| regionsFile | String | *empty list* | To specify regions to impute, you can also give the file path of a .bed file (tabulated file without header: chr1 \t start \t end ) |
| excludedRegionsFile | String | *empty* | To specify regions you don't want to impute, you can also give the file path of a .bed file (tabulated file without header: chr1 \t start \t end ) |
| minReadsVariant | Integer | *empty* | When reading the input file, if the total reads of the variant < minReads, then the variant is filtered out. |
| maxReadsVariant | Integer | *empty* | When reading the input file, if the total reads of the variant < minReads, then the variant is filtered out. |
| minReadsDP | Integer | 1 | When reading the input file, if depth (DP) of a site for a sample is < minReads, then the genotype is set to Missing Data. |
| maxReadsDP | Integer | 10*mean of the sample reads number | When reading the input file, if depth (DP) of a site for a sample is > maxReads, then the genotype is set to Missing Data. |
| filterVariantsOnGenotypicFrequencies | Boolean | true | Filter variants on minimum and maximum frequencies. The minimum and maximum frequencies are automatically calculated from the input vcf data. If set to false, skips the step. One can specify their own values for the min and max allelic frequencies in the `dev` version of the properties file. |
| filterOutRedundant | Boolean | false | Remove redundant loci, defined as contigous loci with exactly the same genotype through the population (except for missing data). If set to false, skips the step.
| correctFalseHTZ | Boolean | false | Correct obvious false heterozygous, defined as sparse heterozygous genotype within homozygous chunks. This parameter is skiped for populations with high expected heterozygosity (e.g. F2, BC1, BC1F3). |
| filterOutIncoherent | Boolean | true | Remove incoherent variants. An incoherant variant is defined as a locus with genotypic frequencies that depart too much from its expected values according to a window that surrounds the tested locus across the whole population. |
| fillMissingDataBefore | Boolean | false | Add a step before the detection of incoherent variants to fill in missing data between 2 same genotypes. Parameter ignored for F2 population. |
| incoherentHalfWindowSize | Integer | round(0.01 * total number of variants) | Number of variants just before and just after each variant to use in calculation of chi2 in order to determine if this variant will be kept or not. Ideally, use a window size (in number of variants) that covers at least half of the expected size of the largest structural variation. If one computes a VCF containing several contigs/chromosomes, then the incoherent window size should be decided according to the specificities of the largest contig/chromosome. We suggest to run Noisy once to have an idea of the number of variants retained after filtering and thus decide a window size. This way, one can specify a number of variants that corresponds to a certain percentage of the overall remaining sites to be used (**i.e. a number corresponding to 2% of the remaining sites).|
| chi2alpha | Double | 0.01 | Chi2 alpha used to determine if a variant is kept or not. This helps filter variants that are too divergent from the expected frequencies defined from what is observed across the population. |
| tryOtherPhase | Boolean | false | tryOtherPhase is used to rescue variants from filtering if their parental genotypes have been incorrectly assigned |

## Imputation

| Parameter | type | Default | description |
|-----------|------|---------|-------------|
| errorA | Double | 0.05 | Specify the probability of observing a read with a B whereas the genotype is truly A. |
| errorB | Double | 0.03 | Specify the probability of observing a read with an A whereas the genotype is truly B. |
| imputeHalfWindowSize | Integer | 20 | Set the number of variants just before and just after each variant used in calculation of allelic frequencies in order to impute.|
| genoLRthreshold | Double | 0.999 | Set a threshold so the genotype is called if its likelihood ratio over the other possible genotypes is > genoLRthreshold. Specifying a high `genoLRthreshold` ensures that the called genotypes are correct. The sites for which no genotype has a P>probaThreashold will be left as "unimputed" because they correspond to recombination points. They will be imputed later by the breakpoints step. |
| alphaFillGaps | Double | 0.01 | Set an alpha threshold so that you fill a gap with a given genotype if its probability < alphaFillGaps. |
| recalculateErrorsBeforeStep3 | Boolean | true | Re-estimate the errorA and errorB based on A, B and H regions defined by first round of imputation compared to raw data from input VCF. |
| breakpointHalfWindowSize | Integer | imputeHalfWindowSize/2 | Set the number of variants just before and just after each variant used in calculation breakpoint probability. |
| alphaBreakpointLooseInterval | Double | 0.05 | Specify an alpha threshold to define the breakpoint loose support interval when the _p-value_ of the likelihood test for genotypes is > alphaLooseInterval |
| dropBreakpointSupportInterval | Double | 1 | Set a threshold corresponding to the left and right positions for which the -log10(Probability that a breakpoint occurs at this given point/Max probability for the breakpoint) drops by the alphaBKP value. |

## Post-imputation filtering

| Parameter | type | Default | description |
|-----------|------|---------|-------------|
| detectImprobableChunks | Boolean | true | Remove improbable chunks, defined as small segments in a given sample that are unlikely to occur given the recombination pattern cross the population. |
| smallChunkMaxSize | Integer | 500 | Define the maximal size of a segment to be considered a chunk. Ideally, use a large window size (in number of variants) to make sure that all chunks will be checked.
| alphaChunk | Double | 0.001 | Set an alpha threshold from which chunks with a probability < alphaChunk will be replaced by their surrounding genotypes. |
| filterOutAlienSegments | Boolean | true | Remove alien segments across the population. An alien segment is defined as a region that causes extreme genetic map expansion. If set to false, skips the step. In order for this option to work properly. |
| alienMaxWindowSize | Integer | 250 | Maximum size of alien segments (in variants) to look for. |
| alienMaxRF | Double | 0.02 | Maximum recombination fraction used to determine the end of an alien segment. |
| slope threshold | slopeCutoff | Integer | Minimum slope required to state the beginning of alien segment. |
| minAlienSize| Integer | 20 | Minimum alien segment size (nb of variants inside the segment).
| maxStartAlienOffset | Integer | 10 | Maximum number of variants located before the alien segment to be considered for the Reconbination Fraction test (RF) for detection of alien segments. |
| collapseGenoMatrix | Boolean | true | Generate a collapsed matrix of imputed variants as final output. If set to false, the complete matrix of imputed variants is in the output file. |
| keepBothBreakpointMarkers | Boolean | true | When collapsing, the variants just before and just after will be kept. If set to false, only the varoant just after the breakpoint is kept. |

## Genetic map calculation

| Parameter | type | Default | description |
|-----------|------|---------|-------------|
| bpPercM | Integer | 240000 | Expected average recombination ratio, defined as physical distance/genetic distance (in base pairs per centimorgan). |
| mappingFunction | String | KOSAMBI | Mapping function used to convert recombination fraction to cM. Arguments can be KOSAMBI, HALDANE or NONE.
| useHTZinSSD | Boolean | true | Calculate genetic map using residual heterozygous sites in SSD (RIL) population. |
| minTransitions | Integer | 1 | Ignore individuals that have less than minTransitions (breakpoints). |
| maxTransitions | Integer | 100 | Ignore individuals that have more than maxTransitions (breakpoints). |

## Developers parameters

The following parameters are available in the "Dev" properties file. This file is mainly used to test the algorithm. Thus, these parameters should not be changed without an enlighten awareness of their consequences.

### File storage

| Parameter | type | Default | description |
|-----------|------|---------|-------------|
| storeRawData | Boolean | true | The program automatically converts the data from the VCF file into a new .json file into a "rawData" directory created by the program. Set this parameter to false to avoid this. |
| readRawDataFromStoringFile | Boolean | true | If the program is run several times on the same VCF file, a .json file with the same name as the VCF file should be present in the "rawData" directory. The program will read this file instead of converting again the VCF. You can set this param to false to force reading from the VCF (will be slower). |
| cleanRawDataDirectory | Boolean | false | Set this parameter to true to clean up the rawData directory and force reading data from the VCF file again. |

### Filter on allele frequencies across the population

| Parameter | Type  | Default | Description |
|-----------|-------|-------------|-------------|
| minFreqMD | Double | *estimated value* | Minimum frequency of missing data at a site across the population. |
| maxFreqMD | Double | *estimated value* | Maximum frequency of missing data at a site across the population. |
| minFreqA | Double | *estimated value* | Minimum frequency of A genotype at a site across the population. |
| maxFreqA | Double | *estimated value* | Maximum frequency of A genotype at a site across the population. |
| minFreqB | Double | *estimated value* | Minimum frequency of B genotype at a site across the population. |
| maxFreqB | Double | *estimated value* | Maximum frequency of B genotype at a site across the population. |
| minFreqH | Double | *estimated value* | Minimum frequency of H genotype at a site across the population. |
| maxFreqH | Double | *estimated value* | Maximum frequency of H genotype at a site across the population. |
| lowPercentileA | Double | 0.025 | xxxxx |
| highPercentileA | Double | 0.975 | xxxxx |
| lowPercentileB | Double | 0.025 | xxxxx |
| highPercentileB | Double | 0.975 | xxxxx |
| lowPercentileH | Double | 0.025 | xxxxx |
| highPercentileH | Double | 0.975 | xxxxx |
| lowPercentileMD | Double | 0.025 | xxxxx |
| highPercentileMD | Double | 0.95 | xxxxx |
| lowPercentileTrans | Double | 0.025 | xxxxx |
| highPercentileTrans | Double | 0.975 | xxxxx |
| correctMinPercentileA | Double | 0.001 | xxxxx |
| correctMaxPercentileA | Double | 0.95 | xxxxx |
| correctMinPercentileB | Double | 0.001 | xxxxx |
| correctMaxPercentileB | Double | 0.95 | xxxxx |
| correctMinPercentileH | Double | 0.001 | xxxxx |
| correctMaxPercentileH | Double | 0.75 | xxxxx |
| correctMinPercentileMD | Double | 0.001 | xxxxx |
| correctMaxPercentileMD | Double | 0.75 | xxxxx |


# Output files

The output file names follow the format "chromosome_stepNumber_content", where chromosome represents the chromosome identifier, stepNumber indicates the program step number, and content is the file content.  

## Main output files

If you choose not to keep all intermediate results (by setting keepIntermediateResults to false), then you will get these files : 

### chr01_GT.vcf (or .csv or .hapmap)
File with imputed data. This file can be a vcf or a csv or a hapmap. For example, if outputFormat is ABH, then you will get something like this :   

| chr   | position | snp         | sample1 | sample2 | sample... |
|-------| -------  | -------     | ------- | ------- | -------   |
| chr01 | 3985     | chr01_3985  | A       | B       | H         |
| chr01 | 3995     | chr01_3995  | A       | B       | H         |
| chr01 | 45847    | chr01_45847 | A       | B       | H         |

### chr01_Breakpoints.csv

List of detected breakpoints with their positions and supplementary information on the determined loose_interval and support_interval

| sample | chr | average_bkp_position | bkp_start_position | bkp_stop_position | found_bkp | bkp_size | loose_interval_start_position | loose_interval_stop_position | found_loose_interval | SNPs_nb_in_loose_interval | data_nb_in_loose_interval | support_interval_start_position | support_interval_stop_position | SNPs_nb_in_support_interval | data_nb_in_support_interval | SNPs_nb_in_HMZ_window | SNPs_nb_in_H_window | transitionType |
|---------| ------| -------  | -------- | -------- |------| ----- |----------|----------|------|----|----|----------|----------|----|---|---|---|--------|
| sample1 | chr01 | 8334408  | 8326363  | 8342452  | true | 16089 | 8294632  | 8355842  | true | 12 | 10 | 8313444  | 8342452  | 3  | 2 | 5 | 5 | A => B |
| sample1 | chr01 | 38582403 | 38582393 | 38582412 | true | 19    | 38577940 | 38586334 | true | 21 | 10 | 38582393 | 38582840 | 5  | 1 | 5 | 5 | B => A |
| sample2 | chr01 | 15215200 | 15215188 | 15215211 | true | 23    | 15213171 | 15216208 | true | 19 | 10 | 15214685 | 15215906 | 11 | 3 | 5 | 5 | A => B |

### chr01_map.csv

| chr   | position | snpName       | nbA | nbB | nbH | ps_cM  | F1Rec  | RF     | cumulated_cM | cMbp    |slope     | slope2     | transitions |
|-------| -------  | -------       | --- | --- | ----|--------| -------| -------| -------------| --------| ---------|------------|-------------|
| chr01	| 1989180  | chr01_1989180 | 111 | 101 | 0   | 8.2882 | 0.0    | 0.0    | 9.99         | 0.0     | 0.0      | 17815.5120 | 0           |
| chr01	| 1989696  | chr01_1989696 | 111 | 101 | 0   | 8.2904 | 0.0    | 0.0    | 9.99         | 898.8764| 73.5632  | 61947.9733 | 1           |
| chr01	| 1989963  | chr01_1989963 | 112 | 100 | 0	 | 8.2915 | 0.0023 | 0.0047 | 10.23        | 0.0     | 202.1052 | -61947.9733| 0           |
| chr01	| 1989981  | chr01_1989981 | 112 | 100 | 0   | 8.2915 | 0.0    | 0.0    | 10.23        | 0.0     | 0.0      | -72504.1302| 0           |

### chr01_SampleStats.csv
Gives for each samples the Percentage of A, B , H or missing data, number of reads and number of transitions (or breakpoints).

| Chr   | Sample    | %MD | %HTZ | %A     | %B     | reads  | transitions_nb |
|-------|-----------|-----|------|--------|--------|--------|----------------|
| chr01 | sample1   | 0.0 | 0.0  | 0.3027 | 0.6972 | 343445 | 2              |
| chr01 | sample2   | 0.0 | 0.0  | 0.7860 | 0.2139 | 320792 | 2              |
| chr01 | sample3   | 0.0 | 0.0  | 0.5158 | 0.4841 | 309702 | 5              |

### chr_SnpGenotypingFrequencies.csv

Gives for each sample the percentage of A, B , H or missing data, number of reads and number of transitions (or breakpoints).

| chr	| position | snp         | A      | B      | H   | MD  | reads |
|-------|----------|-------------|--------|--------|-----|-----|-------|
| chr01	| 3985	   | chr01_3985  | 0.4858 | 0.5141 | 0.0 | 0.0 | 505   |
| chr01	| 3995     | chr01_3995  | 0.4858 | 0.5141 | 0.0 | 0.0 | 523   |
| chr01	| 45847	   | chr01_45847 | 0.4858 | 0.5141 | 0.0 | 0.0 | 464   |

N.B. If you set collapseGenoMatrix to true, then you will also get 2 additional files which are chr_map_collapsed.csv and chr_GT_afterCollapsing.vcf.

### parameters.txt
Stores all the parameters values that were effectively used by the program

## Intermediate output files

Here is the list of possible output files you will get if you set keepIntermediateResults to true.  

| File name  | Extension       | Description |
|------------|-----------------|-------------|
| chr01_0_GT or chr01_0_GTafterFilteringOnRegions |vcf, csv, hapmap | Genotypes of all raw data or without filtered regions (depending on given parameters) |
| chr01_0_GT_SnpGenotypicFrequencies | csv | Genotypic frequencies for each snp |
| chr01_1_GTafterFilteringOnReads | vcf, csv, hapmap | Genotypes after the pre-imputation step of filtering on reads |
| chr01_1_nbFilteredDPonReadsPerSample| csv | For each sample, gives the number of snps that were set to missing data (filteredDP) and the number of snps that were not missing data (totalDP)|
| chr01_2_GTafterFilteringSnpOut | vcf, csv, hapmap | Genotypes after the pre-imputation step of filtering on reads  |
| chr01_3_GTafterFilteringIncoherentSnpsOut |vcf, csv, hapmap | Genotypes after the pre-imputation step of filtering incoherent snps  |
| chr01_4.1_GTafterImputation | vcf, csv, hapmap | Genotypes after the first imputation step |
| chr01_4.2_GTafterImputation | vcf, csv, hapmap | Genotypes after the second imputation step |
| chr01_4.3_GTafterImputation | vcf, csv, hapmap | Genotypes after the third imputation step |
| chr01_4.3_Breakpoints | csv | Positions of detected breakpoints after the third imputation step |
| chr01_5_GTafterRemovingImprobableChunks | vcf, csv, hapmap | Genotypes after removing improbable chunks |
| chr01_5_Chunks.csv | csv | Removed improbable chunks |
| chr01_5_Breakpoints | csv | Positions of detected breakpoints after removing improbable chunks |
| chr01_6_GTafterCollapsing | vcf, csv, hapmap | Genotypes of collapsed data after chunks step |
| chr01_6_map | csv | Genetic map calculated on collapsed data and used for alien detection |
| chr01_6_GTafterRemovingAliens | vcf, csv, hapmap | Genotypes on collapsed data after removing aliens (only if aliens were detected) |
| chr01_6_Aliens | csv | detected aliens positions (only if aliens were detected) |
| chr01_final_GTafterCollapsing | vcf, csv, hapmap | Genotypes of final collapsed data |
| chr01_final_GT | vcf, csv, hapmap | Genotypes of final imputed data |
| chr01_final_map_collapsed| csv | Final genetic map on final imputed collapsed data |
| chr01_final_map | csv | Genetic map on final imputed data |
| chr01_final_breakpoints | csv | Positions of detected breakpoints |
